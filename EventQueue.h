/*
 * EventQueue.h
 *
 *  Created on: Jan 14, 2018
 *      Author: rwbazillion
 */

#ifndef EVENTQUEUE_H_
#define EVENTQUEUE_H_

#include <iostream>
#include <pthread.h>
#include <list>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>


using namespace std;

template<typename T>
class EventQueue
{
public:

	EventQueue(bool blocking, int timeoutMS=100)
            : m_mutex(PTHREAD_MUTEX_INITIALIZER)
			, m_msgSent(PTHREAD_COND_INITIALIZER)
			, m_condition(false)
			, m_blocking(blocking)
			, m_timeoutMS(100)
			{}
	~EventQueue() {}
	void SendMessage(const T& x)
	{
		pthread_mutex_lock(&m_mutex);
		if (m_blocking)
		{
			m_condition = true;
		}
		m_list.push_back(x);
		pthread_cond_signal(&m_msgSent);
		pthread_mutex_unlock(&m_mutex);
	}
	T ReceiveMessage()
	{
		pthread_mutex_lock(&m_mutex);

		T tmp;

		if (m_blocking)
		{
			while (!m_condition)
			{
					//wait for the condition1
					pthread_cond_wait(&m_msgSent, &m_mutex);
			}
			m_condition = false;
			tmp = m_list.front();
			m_list.pop_front();
		}
		else
		{
			int result;

			struct timeval now;
			gettimeofday(&now,NULL);

			/* Set the Timeout */
			struct timespec	ts;
			ts.tv_sec = 0;
			ts.tv_nsec = (now.tv_usec+1000UL*m_timeoutMS)*1000UL;

			//wait for the condition1
			result = pthread_cond_timedwait(&m_msgSent, &m_mutex, &ts);

			if ( (result != ETIMEDOUT &&	//not timeout
				  result != EINVAL &&		//not invalid cond or mutex
				  result != EPERM) &&		//not mutex owned by another thread
				  !m_list.empty() )			//not empty
			{
				tmp = m_list.front();
				m_list.pop_front();
			}
		}

		pthread_mutex_unlock(&m_mutex);

		return(tmp);
	}

protected:

private:

	std::list<T> m_list;
	pthread_mutex_t m_mutex;
	pthread_cond_t m_msgSent;
	bool m_condition;
	bool m_blocking;
	int m_timeoutMS;
};


#endif /* EVENTQUEUE_H_ */
