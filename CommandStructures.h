/*
 * CommandStructures.h
 *
 *  Created on: Jan 15, 2018
 *      Author: rwbazillion
 */

#ifndef COMMANDSTRUCTURES_H_
#define COMMANDSTRUCTURES_H_

//#define MED_DEBUG

//*************************** enum **********************************
//*******************************************************************

typedef enum
{
	CHANGE_HR = 1,
	GEN_AFIB,

}GEN_COMMAND;

typedef enum
{
	PRINT_HR = 1,
	PRINT_LIGHTS,
	PRINT_AUDIBLE,
	PRINT_ALPHA,
	PRINT_ALL,

}PRINT_COMMAND;

typedef enum
{
	CAL_AND_FILTER = 1,

}CAL_AND_FILTER_COMMAND;

typedef enum
{
	BEAT_PATTERN = 1,
	BEAT_FILTER_TOGGLE,
	GET_FILTER_ENABLE,

}BEAT_PATTERN_COMMAND;

typedef enum
{
	HEARTRATE_PRINT = 1,
	HEARTRATE_VALUE,

}DISPLAY_HEARTRATE_COMMAND;

typedef enum
{
	CONTROL_LIGHTS_PRINT = 1,
	CONTROL_LIGHTS_VALUE,

}CONTROL_LIGHTS_COMMAND;

typedef enum
{
	CONTROL_AUDIBLE_PRINT = 1,
	CONTROL_AUDIBLE_VALUE,

}CONTROL_AUDIBLE_COMMAND;

typedef enum
{
	DISPLAY_ALPHA_PRINT = 1,
	DISPLAY_ALPHA_VALUE,

}DISPLAY_ALPHA_COMMAND;

typedef enum
{
	RED = 1,
	YELLOW,
	GREEN,

}CONTROL_LIGHTS;

typedef enum
{
	SIREN_BEEP = 1,
	WARNING_BEEP,
	GOOD_BEEP,

}CONTROL_AUDIBLE;

//*************************** structs *******************************
//*******************************************************************

typedef struct
{

	GEN_COMMAND hr_command;
	int value;

}STR_HR_DATA;

typedef struct
{
	PRINT_COMMAND printCommand;

}STR_PR_DATA;

typedef struct
{
	CAL_AND_FILTER_COMMAND calAndFilterCommand;
	int value;

}STR_CF_DATA;

typedef struct
{
	BEAT_PATTERN_COMMAND beatPatternCommand;
	int value;

}STR_BP_DATA;

typedef struct
{
	DISPLAY_HEARTRATE_COMMAND displayHeartRateCommand;
	int value;

}STR_DH_DATA;

typedef struct
{
	CONTROL_LIGHTS_COMMAND controlLightsCommand;
	CONTROL_LIGHTS value;

}STR_CL_DATA;

typedef struct
{
	CONTROL_AUDIBLE_COMMAND controlAudibleCommand;
	CONTROL_AUDIBLE value;

}STR_CA_DATA;

typedef struct
{
	DISPLAY_ALPHA_COMMAND displayAlphaCommand;
	int value;

}STR_DA_DATA;

#endif /* COMMANDSTRUCTURES_H_ */
