/*
 * Printer.cpp
 *
 *  Created on: Jan 14, 2018
 *      Author: rwbazillion
 */

#include <iostream>
#include "Printer.h"
#include "EventQueue.h"
#include "CommandStructures.h"

using namespace std;

extern EventQueue<STR_PR_DATA> printerMsgQueue;
extern EventQueue<STR_DH_DATA> displayHeartRateMsgQueue;
extern EventQueue<STR_CL_DATA> controlLightsMsgQueue;
extern EventQueue<STR_CA_DATA> controlAudibleMsgQueue;
extern EventQueue<STR_DA_DATA> displayAlphaMsgQueue;

void* Printer::PrinterThread(void* arg)
{
	STR_PR_DATA prData;
	STR_DH_DATA dhData;
	STR_CL_DATA clData;
	STR_CA_DATA caData;
	STR_DA_DATA daData;

	for (;;)
	{
		prData = printerMsgQueue.ReceiveMessage();

		switch (prData.printCommand)
		{
			case PRINT_HR:
				dhData.displayHeartRateCommand = HEARTRATE_PRINT;
				displayHeartRateMsgQueue.SendMessage(dhData);

#ifdef MED_DEBUG
				cout << "Print HR command Received" << endl;
#endif
				break;

			case PRINT_LIGHTS:
				clData.controlLightsCommand = CONTROL_LIGHTS_PRINT;
				controlLightsMsgQueue.SendMessage(clData);

#ifdef MED_DEBUG
				cout << "Print Lights command Received" << endl;
#endif
				break;

			case PRINT_AUDIBLE:
				caData.controlAudibleCommand = CONTROL_AUDIBLE_PRINT;
				controlAudibleMsgQueue.SendMessage(caData);

#ifdef MED_DEBUG
				cout << "Print Audible command Received" << endl;
#endif
				break;

			case PRINT_ALPHA:
				daData.displayAlphaCommand = DISPLAY_ALPHA_PRINT;
				displayAlphaMsgQueue.SendMessage(daData);

#ifdef MED_DEBUG
				cout << "Print Alpha command Received" << endl;
#endif
				break;

			case PRINT_ALL:
				dhData.displayHeartRateCommand = HEARTRATE_PRINT;
				displayHeartRateMsgQueue.SendMessage(dhData);
				clData.controlLightsCommand = CONTROL_LIGHTS_PRINT;
				controlLightsMsgQueue.SendMessage(clData);
				caData.controlAudibleCommand = CONTROL_AUDIBLE_PRINT;
				controlAudibleMsgQueue.SendMessage(caData);
				daData.displayAlphaCommand = DISPLAY_ALPHA_PRINT;
				displayAlphaMsgQueue.SendMessage(daData);

#ifdef MED_DEBUG
				cout << "Print All command Received" << endl;
#endif
				break;
		}
	}
	return NULL;
}
