/*
 * PulseObject.cpp
 *
 *  Created on: January 12, 2018
 *      Author: rwbazillion
 *
 * REFERENCES:
 * https://stackoverflow.com/questions/19758101/signal-handler-to-stop-timer-in-c
 * http://man7.org/linux/man-pages/man2/setitimer.2.html
 * http://man7.org/linux/man-pages/man7/signal.7.html
 *
 */

#include <iostream>
#include <cstdio>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include "HeartRateGenerator.h"
#include "EventQueue.h"
#include "CommandStructures.h"

#define DEFAULT_HR 70
#define AFIB_HR 45

static pthread_mutex_t sMutex_HR;
static int sHeartRate;
static int sTempHeartRate;

using namespace std;

extern EventQueue<STR_HR_DATA> heartRateGeneratorMsgQueue;
extern EventQueue<STR_CF_DATA> calculateAndFilterMsgQueue;

HeartRateGenerator::HeartRateGenerator( void )
{
	sHeartRate = DEFAULT_HR;
	sMutex_HR = PTHREAD_MUTEX_INITIALIZER;
}

void HeartRateGenerator::TimerExpired(int signum)
{
	pthread_mutex_lock(&sMutex_HR);
	sHeartRate = sTempHeartRate;
	pthread_mutex_unlock(&sMutex_HR);

	cout << endl << "AFIB Expired heart rate is at " << sHeartRate << " bpm." << endl;

}

void* HeartRateGenerator::HeartRatePulser(void* arg)
{
	STR_CF_DATA cfData;

	//Send Heart rate to Calculate & Filter Queue
	cfData.calAndFilterCommand = CAL_AND_FILTER;

	for (;;)
	{
		pthread_mutex_lock(&sMutex_HR);
		cfData.value = sHeartRate;
		pthread_mutex_unlock(&sMutex_HR);

		calculateAndFilterMsgQueue.SendMessage(cfData);
		sleep(1);
	}

	return NULL;
}

void* HeartRateGenerator::HeartRateThread(void* arg)
{
	STR_HR_DATA hrData;

	pthread_t thread_HR;
	pthread_attr_t attr;

	//initialize attributes
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	//Create Heart Rate pulser thread
	pthread_create(&thread_HR, &attr, &HeartRatePulser, NULL);

	//Creating timer interval and setting time
	struct itimerval it_val;
    it_val.it_value.tv_sec = 10;
    it_val.it_value.tv_usec = 0;
    it_val.it_interval.tv_sec = 0;
    it_val.it_interval.tv_usec = 0;

    if (signal(SIGALRM, TimerExpired) == SIG_ERR) {
        perror("Unable to catch SIGALRM");
    }

	for (;;)
	{
		hrData = heartRateGeneratorMsgQueue.ReceiveMessage();

		switch (hrData.hr_command)
		{
			case CHANGE_HR:
				pthread_mutex_lock(&sMutex_HR);
				sHeartRate = hrData.value;
				pthread_mutex_unlock(&sMutex_HR);
				cout << "new heart rate is " << hrData.value << endl;
				break;

			case GEN_AFIB:
				cout << "generating AFIB for 10 seconds " << endl;
				pthread_mutex_lock(&sMutex_HR);
				sTempHeartRate = sHeartRate;
				sHeartRate = AFIB_HR;
				pthread_mutex_unlock(&sMutex_HR);
				if (setitimer(ITIMER_REAL, &it_val, NULL) == -1)
					perror("error calling setitimer()");
				break;

			default:
				break;
		}
	}

	pthread_join(thread_HR, NULL);
	pthread_attr_destroy(&attr);
	pthread_exit(NULL);

	return NULL;
}

#ifdef TIMER_USING_ANOTHER_METHOD
/*
 * This is reference code for future timer development. We need to learn how to
 * setup a timer using this method. We were able to do it but it kept messing up cin
 * in the main thread. This needs to be investigated.
 *
 * References:
 *
 * TIMERS
 * https://www.systutorials.com/docs/linux/man/7-signal/
 * https://www.systutorials.com/docs/linux/man/2-sigaction/
 * https://stackoverflow.com/questions/20684290/signal-handling-and-sigemptyset
 * https://arstechnica.com/civis/viewtopic.php?f=20&t=126117
 * https://en.wikipedia.org/wiki/Signal_(IPC)
 * https://en.wikipedia.org/wiki/C_signal_handling
 * http://www.qnx.com/developers/docs/6.4.1/neutrino/getting_started/s1_timer.html
 * http://www.linuxprogrammingblog.com/all-about-linux-signals?page=2
 *
 */

void HeartRateGenerator::HeartRateAfibTimerExpired(int sig, siginfo_t *si, void *uc)
{
	pthread_mutex_lock(&sMutex_HR);
	sHeartRate = sTempHeartRate;
	pthread_mutex_unlock(&sMutex_HR);

	cout << endl << "AFIB Expired heart rate is at " << sHeartRate << " bpm." << endl;

}

//Setup AFIB Timer START
//***********************
//the process of starting a timer
//
//1) Create the timer object.
//2) Decide how you wish to be notified (signal, pulse, or thread creation),
//   and create the notification structure (the struct sigevent).
//3) Decide what kind of timer you wish to have (relative versus absolute,
//   and one-shot versus periodic).
//4) Start it.

// Setup the timer (1)
//	struct itimerspec	itsAfib;
//	itsAfib.it_value.tv_sec = 10;
//	itsAfib.it_value.tv_nsec = 0;
//	itsAfib.it_interval.tv_sec = 0;
//	itsAfib.it_interval.tv_nsec = 0;

//Decide how you wish to be notified (2)

/* Establish handler for timer signal */
//	struct sigaction saAfib;
//	saAfib.sa_sigaction = HeartRateAfibTimerExpired;
//	sigemptyset(&saAfib.sa_mask);
//	saAfib.sa_flags = SA_SIGINFO;
//
//	// Change the action taken by a process on receipt of a specific signal.
//	sigaction(SIGALRM, &saAfib, NULL);

//Decide what kind of timer you wish to have (3)

//Setup timer ID
//	timer_t timerAfibid;

// Create the timer signal event
//	struct sigevent sevAfib;
//	sevAfib.sigev_notify = SIGEV_SIGNAL;
//	sevAfib.sigev_signo = SIGALRM;
//	sevAfib.sigev_value.sival_ptr = &timerAfibid;
//	timer_create(CLOCK_REALTIME, &sevAfib, &timerAfibid);

//Start it. -- Done if GEN_AFIB is received (4)

//***********************
//Setup AFIB Timer END

//				timer_settime (timerAfibid, 0, &itsAfib, NULL);

#endif
