/*
 * BeatPattern.cpp
 *
 *  Created on: Jan 14, 2018
 *      Author: rwbazillion
 */

#include <iostream>
#include "BeatPattern.h"
#include "EventQueue.h"
#include "CommandStructures.h"

#define MAX_LIST 60

static int sBeatPatternList[MAX_LIST];

using namespace std;

extern EventQueue<STR_BP_DATA> beatPatternMsgQueue;
extern EventQueue<STR_CL_DATA> controlLightsMsgQueue;
extern EventQueue<STR_CA_DATA> controlAudibleMsgQueue;
extern EventQueue<STR_DA_DATA> displayAlphaMsgQueue;

void* BeatPattern::BeatPatternThread(void* arg)
{
	STR_BP_DATA bpData;
	STR_CL_DATA clData;
	STR_CA_DATA caData;
	STR_DA_DATA daData;

	int* currentPos;

	int fullIndex = 1;
	int index = 0;
	int sum = 0;
	int finalValue = 0;

	bool filterEnable = false;

	currentPos = &sBeatPatternList[0];

	for (;;)
	{
		bpData = beatPatternMsgQueue.ReceiveMessage();

		switch (bpData.beatPatternCommand)
		{
			case BEAT_PATTERN:
			{
				clData.controlLightsCommand = CONTROL_LIGHTS_VALUE;
				caData.controlAudibleCommand = CONTROL_AUDIBLE_VALUE;
				daData.displayAlphaCommand = DISPLAY_ALPHA_VALUE;

				if (filterEnable)
				{
					if (index < MAX_LIST)
					{
						*(currentPos + index) = bpData.value;
						index++;
						if (fullIndex < MAX_LIST)
						{
							fullIndex++;
						}
					}
					else
					{
						index = 0;
					}

					for (int i=0; i<fullIndex; i++)
					{
						sum += *(currentPos + index);
					}

					finalValue = sum / fullIndex;
				}
				else
				{
					finalValue = bpData.value;
				}

				if ((finalValue > 0 && finalValue <= 40) ||
					(finalValue > 200) ||
					(finalValue <= 0))
				{
					clData.value = RED;
					caData.value = SIREN_BEEP;
					daData.value = finalValue ;
				}
				else if ((finalValue > 41  && finalValue <= 50) ||
						(finalValue > 160  && finalValue <= 200))
				{
					clData.value = YELLOW;
					caData.value = WARNING_BEEP;
					daData.value = finalValue ;
				}
				else if (finalValue > 50  && finalValue <= 160)
				{
					clData.value = GREEN;
					caData.value = GOOD_BEEP;
					daData.value = finalValue ;
				}

#ifdef MED_DEBUG
				cout << "Beat Pattern " << bpData.value << " Received" << endl;
#endif

				controlLightsMsgQueue.SendMessage(clData);
				controlAudibleMsgQueue.SendMessage(caData);
				displayAlphaMsgQueue.SendMessage(daData);

				sum = 0;
				finalValue = 0;

				break;
			}
			case BEAT_FILTER_TOGGLE:
			{
				if (filterEnable)
					filterEnable = false;
				else
					filterEnable = true;

				break;
			}
			case GET_FILTER_ENABLE:
			{
				cout << "Beat Pattern Filter is " << ((filterEnable == false) ? "false" : "true");
				cout << endl;

				break;
			}
		}
	}
	return NULL;
}



