/*
 * PulseObject.h
 *
 *  Created on: January 12, 2018
 *      Author: rwbazillion
 *
 *
 */

#ifndef HEARTRATEGENERATOR_H
#define HEARTRATEGENERATOR_H

#include <signal.h>

class HeartRateGenerator
{
public:

	HeartRateGenerator();

	static void TimerExpired(int signum);

	static void* HeartRatePulser(void* arg);

	static void* HeartRateThread(void* arg);

protected:

private:

};

#endif /* HEARTRATEGENERATOR_H */

#ifdef TIMER_USING_ANOTHER_METHOD
	static void HeartRateAfibTimerExpired(int sig, siginfo_t *si, void *uc);
#endif
