/*
 * main.cpp
 *
 *  Created on: January 12, 2018
 *      Author: rwbazillion
 *
 *
 */

#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>

#include "CalculateAndFilter.h"
#include "HeartRateGenerator.h"
#include "Printer.h"
#include "BeatPattern.h"
#include "DisplayDrivers.h"
#include "EventQueue.h"
#include "CommandStructures.h"

using namespace std;

#define NUMTHRDS 8

//Queues for tasks
EventQueue<STR_HR_DATA> heartRateGeneratorMsgQueue(true);
EventQueue<STR_PR_DATA> printerMsgQueue(true);
EventQueue<STR_CF_DATA> calculateAndFilterMsgQueue(true);
EventQueue<STR_BP_DATA> beatPatternMsgQueue(true);
EventQueue<STR_DH_DATA> displayHeartRateMsgQueue(true);
EventQueue<STR_CL_DATA> controlLightsMsgQueue(true);
EventQueue<STR_CA_DATA> controlAudibleMsgQueue(true);
EventQueue<STR_DA_DATA> displayAlphaMsgQueue(true);

int main()
{
	pthread_t threads[NUMTHRDS];
	pthread_attr_t attr;

	//initialize attributes
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	HeartRateGenerator heartRateObj;
	CalculateAndFilter calculateAndFilterObj;
	Printer printerObj;
	BeatPattern beatPatternObj;
	DisplayHR displayHeartRateObj;
	ControlLights controlLightsObj;
	ControlAudible controlAudibleObj;
	DisplayAlpha displayAlphaObj;

	//Creates threads.
	pthread_create(&threads[0], &attr, &heartRateObj.HeartRateThread, NULL);
	pthread_create(&threads[1], &attr, &calculateAndFilterObj.CalculateAndFilterThread, NULL);
	pthread_create(&threads[2], &attr, &printerObj.PrinterThread, NULL);
	pthread_create(&threads[3], &attr, &beatPatternObj.BeatPatternThread, NULL);
	pthread_create(&threads[4], &attr, &displayHeartRateObj.DisplayHR_Thread, NULL);
	pthread_create(&threads[5], &attr, &controlLightsObj.ControlLightsThread, NULL);
	pthread_create(&threads[6], &attr, &controlAudibleObj.ControlAudibleThread, NULL);
	pthread_create(&threads[7], &attr, &displayAlphaObj.DisplayAlphaThread, NULL);

	int sel, newHr;
	STR_HR_DATA hrData;
	STR_PR_DATA prData;
	STR_BP_DATA bpData;

	for (;;)
	{

		cout << endl;
		cout << "Select command 1 for change heart rate" << endl;
		cout << "Select command 2 to generate an afib" << endl;
		cout << "Select command 3 to print current HR" << endl;
		cout << "Select command 4 to print light" << endl;
		cout << "Select command 5 to print audible" << endl;
		cout << "Select command 6 to print alphanumeric display" << endl;
		cout << "Select command 7 to print ALL" << endl;
		cout << "Select command 8 to toggle beatpattern filter" << endl;
		cout << "Select command 9 to get beatpattern filter enable" << endl;
		cout << endl;
		cout << "select and press [enter]: ";
		cout << endl;
		cin >> sel;

		switch(sel)
		{
			case 1:
				cout << "\nSelect new heart rate\n";
				hrData.hr_command = CHANGE_HR;
				cin >> newHr;
				hrData.value = newHr;
				heartRateGeneratorMsgQueue.SendMessage(hrData);
				break;
			case 2:
				hrData.hr_command = GEN_AFIB;
				heartRateGeneratorMsgQueue.SendMessage(hrData);
				break;
			case 3:
				prData.printCommand = PRINT_HR;
				printerMsgQueue.SendMessage(prData);
				break;
			case 4:
				prData.printCommand = PRINT_LIGHTS;
				printerMsgQueue.SendMessage(prData);
				break;
			case 5:
				prData.printCommand = PRINT_AUDIBLE;
				printerMsgQueue.SendMessage(prData);
				break;
			case 6:
				prData.printCommand = PRINT_ALPHA;
				printerMsgQueue.SendMessage(prData);
				break;
			case 7:
				prData.printCommand = PRINT_ALL;
				printerMsgQueue.SendMessage(prData);
				break;
			case 8:
				bpData.beatPatternCommand = BEAT_FILTER_TOGGLE;
				beatPatternMsgQueue.SendMessage(bpData);
				break;
			case 9:
				bpData.beatPatternCommand = GET_FILTER_ENABLE;
				beatPatternMsgQueue.SendMessage(bpData);
				break;

		}
	}

	//Wait for all threads to finish
	for (int i=0; i<NUMTHRDS; i++)
	{
		pthread_join(threads[i], NULL);
	}
	pthread_attr_destroy(&attr);
	pthread_exit(NULL);

	return 0;
}
