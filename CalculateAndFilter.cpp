/*
 * calculateAndFilterTask.cpp
 *
 *  Created on: Jan 14, 2018
 *      Author: rwbazillion
 */

#include <iostream>
#include "CalculateAndFilter.h"
#include "EventQueue.h"
#include "CommandStructures.h"

using namespace std;

extern EventQueue<STR_CF_DATA> calculateAndFilterMsgQueue;
extern EventQueue<STR_BP_DATA> beatPatternMsgQueue;
extern EventQueue<STR_DH_DATA> displayHeartRateMsgQueue;

void* CalculateAndFilter::CalculateAndFilterThread(void* arg)
{
	STR_CF_DATA cfData;
	STR_BP_DATA bpData;
	STR_DH_DATA dhData;

	for (;;)
	{
		cfData = calculateAndFilterMsgQueue.ReceiveMessage();

		switch (cfData.calAndFilterCommand)
		{
			case CAL_AND_FILTER:
				bpData.beatPatternCommand = BEAT_PATTERN;
				bpData.value = cfData.value;

				dhData.displayHeartRateCommand = HEARTRATE_VALUE;
				dhData.value = cfData.value;

#ifdef MED_DEBUG
				cout << "Calculate and Filter " << cfData.value << "Pulse Received" << endl;
#endif
				beatPatternMsgQueue.SendMessage(bpData);
				displayHeartRateMsgQueue.SendMessage(dhData);

				break;
		}
	}
	return NULL;
}
