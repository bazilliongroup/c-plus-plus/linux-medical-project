/*
 * DisplayDrivers.h
 *
 *  Created on: Jan 14, 2018
 *      Author: rwbazillion
 */

#ifndef DISPLAYDRIVERS_H_
#define DISPLAYDRIVERS_H_

class DisplayHR
{
public:

	static void* DisplayHR_Thread(void* arg);

protected:

private:

};

class ControlLights
{
public:

	static void* ControlLightsThread(void* arg);

protected:

private:

};

class ControlAudible
{
public:

	static void* ControlAudibleThread(void* arg);

protected:

private:

};

class DisplayAlpha
{
public:

	static void* DisplayAlphaThread(void* arg);

protected:

private:

};

#endif /* DISPLAYDRIVERS_H_ */
