/*
 * BeatPattern.h
 *
 *  Created on: Jan 14, 2018
 *      Author: rwbazillion
 */

#ifndef BEATPATTERN_H_
#define BEATPATTERN_H_

class BeatPattern
{
public:

	static void* BeatPatternThread(void* arg);

protected:

private:

};



#endif /* BEATPATTERN_H_ */
