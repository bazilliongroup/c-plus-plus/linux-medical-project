/*
 * DisplayDrivers.cpp
 *
 *  Created on: Jan 14, 2018
 *      Author: rwbazillion
 */


#include <iostream>
#include "DisplayDrivers.h"
#include "CommandStructures.h"
#include "EventQueue.h"

using namespace std;

extern EventQueue<STR_DH_DATA> displayHeartRateMsgQueue;
extern EventQueue<STR_CL_DATA> controlLightsMsgQueue;
extern EventQueue<STR_CA_DATA> controlAudibleMsgQueue;
extern EventQueue<STR_DA_DATA> displayAlphaMsgQueue;

static int sThreeDigitHeartRate;
static CONTROL_LIGHTS sControlLights;
static CONTROL_AUDIBLE sControlAudible;
static int sAlphaValue;


void* DisplayHR::DisplayHR_Thread(void* arg)
{
	STR_DH_DATA dhData;

	for (;;)
	{
		dhData = displayHeartRateMsgQueue.ReceiveMessage();

		switch(dhData.displayHeartRateCommand)
		{
			case HEARTRATE_PRINT:
				cout << "The heart rate is " << sThreeDigitHeartRate << endl;
				break;
			case HEARTRATE_VALUE:
				sThreeDigitHeartRate = dhData.value;
				break;
			default:
				break;
		}
	}
	return NULL;
}

void* ControlLights::ControlLightsThread(void* arg)
{
	STR_CL_DATA clData;

	for (;;)
	{
		clData = controlLightsMsgQueue.ReceiveMessage();

		switch(clData.controlLightsCommand)
		{
			case CONTROL_LIGHTS_PRINT:
				if (sControlLights == RED)
					cout << "The control light is RED " << endl;
				else if (sControlLights == YELLOW)
					cout << "The control light is YELLOW " << endl;
				else if (sControlLights == GREEN)
					cout << "The control light is GREEN " << endl;
				break;
			case CONTROL_LIGHTS_VALUE:
				sControlLights = clData.value;
				break;
			default:
				break;
		}
	}
	return NULL;
}

void* ControlAudible::ControlAudibleThread(void* arg)
{
	STR_CA_DATA caData;

	for (;;)
	{
		caData = controlAudibleMsgQueue.ReceiveMessage();

		switch(caData.controlAudibleCommand)
		{
			case CONTROL_AUDIBLE_PRINT:
				if (sControlAudible == SIREN_BEEP)
					cout << "The control audible is Emergency" << endl;
				else if (sControlAudible == WARNING_BEEP)
					cout << "The control audible is Warning" << endl;
				else if (sControlAudible == GOOD_BEEP)
					cout << "The control audible is Normal" << endl;
				break;
			case CONTROL_AUDIBLE_VALUE:
				sControlAudible = caData.value;
				break;
			default:
				break;
		}
	}
	return NULL;
}

void* DisplayAlpha::DisplayAlphaThread(void* arg)
{
	STR_DA_DATA daData;

	for (;;)
	{
		daData = displayAlphaMsgQueue.ReceiveMessage();

		switch(daData.displayAlphaCommand)
		{
			case DISPLAY_ALPHA_PRINT:
				cout << "The 60 sec. Average Rate Average is " << sAlphaValue << endl;
				break;
			case DISPLAY_ALPHA_VALUE:
				sAlphaValue = daData.value;
				break;
			default:
				break;
		}

	}
	return NULL;
}


